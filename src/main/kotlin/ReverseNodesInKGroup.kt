import ForTasks.ListNode
//https://leetcode.com/problems/reverse-nodes-in-k-group/
//25 in leetcode
//Given the head of a linked list, reverse the nodes of the list k at a time, and return the modified list.
//
//k is a positive integer and is less than or equal to the length of the linked list.
// If the number of nodes is not a multiple of k then left-out nodes, in the end, should remain as it is.
//
//You may not alter the values in the list's nodes, only nodes themselves may be changed.
class ReverseNodesInKGroup {
    class Solution {
        fun reverseKGroup(head: ListNode?, k: Int): ListNode? {
            var top = head//return new head
            //var ret = head
            //if (head.toString().length < k) return head
            var prev : ListNode? = null
           // while (ret?.next != null) {
            var block = top
            //var haveBlock = true
            while (true) {
                //get a block
                var findBlock = block // find block k elements
                //check if there is K elements in next Block
                var cnt = 0
                for (i in 1..k) {
                    findBlock = findBlock?.next
                    println("findblock = $findBlock")
                    if (findBlock == null) {
                        break
                    }
                    cnt++
                }
                if (findBlock == null) {
                    println("first = null and top = $top")
                    break
                }
                block = findBlock
                //new block found need sort it
            }

            for (y in 1..k-1) {
                for (x in 1..k-y) {
                    var tmp = top
                    prev = null
                    for (i in x..k-2) {
                        //for (i in 1..x) {
                        prev = tmp
                        tmp = tmp?.next
                    }
                    //tmp i k element
                    var first = tmp
                    var second = first?.next
                    var third = second?.next

                    first?.next = third
                    second?.next = first
                    prev?.next = second
                    if (prev == null) {
                        top = second
                    }
                    println("prev = ${prev?.toStringSimple()} first = ${first?.toStringSimple()} second=${second?.toStringSimple()} third=${third?.toStringSimple()}")
                }

            }
            return top
        }
    }
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            var node = ListNode(1)
            node.next = ListNode(2)
            node.next?.next = ListNode(3)
            node.next?.next?.next = ListNode(4)
            node.next?.next?.next?.next = ListNode(5)
            node.next?.next?.next?.next?.next = ListNode(6)
            node.next?.next?.next?.next?.next?.next = ListNode(7)
            node.next?.next?.next?.next?.next?.next?.next = ListNode(8)

            println("node = $node")
            val res = Solution()
            val k = 4
            val result = res.reverseKGroup(node, k)
            println("$result")

        }
    }
}