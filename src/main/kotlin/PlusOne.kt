import kotlin.math.roundToInt
//https://leetcode.com/problems/plus-one/
//66 in leetcode
//You are given a large integer represented as an integer array digits, where each digits[i]
// is the ith digit of the integer. The digits are ordered from most significant to
// least significant in left-to-right order. The large integer does not contain any leading 0's.
//
//Increment the large integer by one and return the resulting array of digits.
class PlusOne {
    class Solution {
        fun plusOne(digits: IntArray): IntArray {
            var sum = 0
            var multiplier = 1
            for (i in digits.size-1 downTo 0) {
                sum += digits[i] * multiplier
                multiplier *= 10
            }


            sum++
            println("sum $sum")

            var list = mutableListOf<Int>()
            var degree = 0
            var prev = 0
            while (true) {
                // next degree of 10
                val currentDegree = (Math.pow(10.0, (degree+1).toDouble())).roundToInt()
                // this is same value without remainder of division, remainder is moved out after div and multiply operations
                var tmp = sum.div(currentDegree)*currentDegree//(value / currentDegree) * currentDegree

                // next digit, but with additional 0's
                var nextDigit = sum - tmp - prev
                // cut value to the digit [0-9]
                nextDigit = nextDigit / Math.pow(10.0, degree.toDouble()).roundToInt()
                //previous is summ of all remainders
                prev += nextDigit * Math.pow(10.0, degree.toDouble()).roundToInt()
                list.add(nextDigit)
                degree++
                if (currentDegree >= sum) {
                    break
                }
            }

            list.reverse()
            return list.toIntArray()
        }
    }
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val arr: IntArray = intArrayOf(1, 2, 3)
            println("arr = ${arr.contentToString()}")
            val res = Solution()
            val result = res.plusOne(arr)
            println("arr with sum +1 = ${result.contentToString()} ${arr.contentToString()}")
        }
    }
}