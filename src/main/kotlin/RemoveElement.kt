//27 in leetcode
//https://leetcode.com/problems/remove-element/
//Given an integer array nums and an integer val, remove all occurrences of val in nums in-place.
// The relative order of the elements may be changed.
//
//Since it is impossible to change the length of the array in some languages,
// you must instead have the result be placed in the first part of the array nums.
// More formally, if there are k elements after removing the duplicates,
// then the first k elements of nums should hold the final result.
// It does not matter what you leave beyond the first k elements.
//
//Return k after placing the final result in the first k slots of nums.
class RemoveElement {
    class Solution {
        fun removeElement(nums: IntArray, value: Int): Int {
            var cnt = 0
            for (i in nums.indices) {

                if (nums[i] != value) {
                    nums[cnt] = nums[i]
                    cnt++
                }
            }
            for (cnt in cnt until nums.size) {
                nums[cnt] = 0
            }
            return cnt
        }
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val nums: IntArray = intArrayOf(0,1,2,2,3,0,4,2)//(3,2,2,3)//
            println("nums = ${nums.contentToString()}")
            val res = Solution()
            val value = 2
            val filtered = nums.filter {
                it != value
            }
            val result = res.removeElement(nums, value)
            println("res = $result nums = ${nums.contentToString()}")
            println("filtered $filtered")

        }
    }
}