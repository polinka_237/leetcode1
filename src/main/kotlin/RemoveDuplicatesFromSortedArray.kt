//26 in leetcode
//https://leetcode.com/problems/remove-duplicates-from-sorted-array/
//Given an integer array nums sorted in non-decreasing order,
// remove the duplicates in-place such that each unique element appears only once.
// The relative order of the elements should be kept the same.
//if there are k elements after removing the duplicates,
// then the first k elements of nums should hold the final result.
// It does not matter what you leave beyond the first k elements.

class RemoveDuplicatesFromSortedArray {
    class Solution {
        fun removeDuplicates(nums: IntArray): Int {
            var cnt = 0
            for (i in nums.indices) {
                if (nums[cnt] != nums[i]) {
                    cnt++
                    nums[cnt] = nums[i]
                }
            }
            for (cnt in cnt+1 until nums.size) {
                nums[cnt] = 0
            }
            return cnt+1
        }

    }
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val nums: IntArray = intArrayOf(1, 1, 2, 2, 3, 4)//(0,0,1,1,1,2,2,3,3,4)////(1, 1, 2)
            println("nums = ${nums.contentToString()}")
            val res = Solution()
            val result = res.removeDuplicates(nums)
            println("res = $result nums = ${nums.contentToString()}")
//            for (i in nums) {
//                println("element $i")
//            }
        }
    }

}