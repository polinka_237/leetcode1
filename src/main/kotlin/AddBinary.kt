//https://leetcode.com/problems/add-binary/
//67 in leetcode
//Given two binary strings a and b, return their sum as a binary string
class AddBinary {
    class Solution {
        fun addBinary(a: String, b: String): String {
            val newA = Integer.parseInt(a, 2)
            val newB = Integer.parseInt(b, 2)
            var sum = Integer.toBinaryString(newA + newB)
            return sum
        }

        fun addBinary2(a: String, b: String): String {
            return a
        }
    }
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val a = "1010"
            val b = "1011"
            val res = Solution()
            val result = res.addBinary(a, b)
            println("result = $result")
        }
    }
}