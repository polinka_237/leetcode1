package ForTasks

class ListNode(var value: Int){

    var next: ListNode? = null

    fun printAll() {
        var top : ListNode? = this
        while (top != null) {
            println("next element ${top.value} $top")
            top = top.next
        }
    }

    override fun toString(): String {
        var str = "["
        var top : ListNode? = this
        while (top != null) {
            str += "${top.value},"
            top = top.next
        }
        str = str.substring(0, str.length-1)
        return "$str]"
    }

    fun toStringSimple(): String {
        return value.toString()
    }
}