//https://leetcode.com/problems/rotate-array/
//Given an integer array nums, rotate the array to the right by k steps, where k is non-negative.
class RotateKArray {
    class Solution {
        fun rotate(nums: IntArray, k: Int): Unit {
            for (i in 0 until k) {
                val tmp = nums[nums.size-1]
                for (j in nums.size-2 downTo 0) {
                    nums[j+1] = nums[j]
                }
                nums[0] = tmp
            }

        }
    }
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val nums: IntArray = intArrayOf(1 ,2, 3, 4, 5, 6, 7)
            println("nums = ${nums.contentToString()}")
            val res = Solution()
            val result = res.rotate(nums, 3)
            println("res = $result nums = ${nums.contentToString()}")

        }
    }
}