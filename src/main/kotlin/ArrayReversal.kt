class ArrayReversal {
    fun reverseArr(arr: IntArray) {


        for (i in 0..(arr.size/2)-1) {
            val tmp = arr[i]
            arr[i] = arr[arr.size-1-i]
            arr[arr.size-1-i] = tmp
        }

    }
    fun reverseArrA(arr: IntArray) {
        val ret = arr.reverse()
        return ret
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val arr: IntArray = intArrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)
            println("arr = ${arr.contentToString()}")
            val res = ArrayReversal()
            res.reverseArr(arr)
           // res.reverseArrA(arr)
            println("arr reverse ${arr.contentToString()}")
        }
    }
}