//80 in leetcode
//https://leetcode.com/problems/remove-duplicates-from-sorted-array-ii/
//Given an integer array nums sorted in non-decreasing order,
// remove some duplicates in-place such that each unique element appears at most twice.
// The relative order of the elements should be kept the same.
//
//Since it is impossible to change the length of the array in some languages,
// you must instead have the result be placed in the first part of the array nums.
// More formally, if there are k elements after removing the duplicates,
//then the first k elements of nums should hold the final result.
//It does not matter what you leave beyond the first k elements.
//
//Return k after placing the final result in the first k slots of nums.

class RemoveDuplicatesFromSortedArray2 {
    class Solution {
        fun removeDuplicates(nums: IntArray): Int {
            var cnt = 0
            var k = 0
            var index = 0
            //for (i in nums.iterator()) {
            while(index < nums.size) {
                k = 0
                var startIndex = -1
                for (j in (index+1)..nums.size-1) {
                    if (nums[index] == nums[j]) {
                        k++
                        if (k == 2) {
                            startIndex = index
                        }
                        index++
                    } else {
                        break
                    }
                }
                println("k = $k for index $index of value ${nums[index]}")

                if (k >= 2) {
                    cnt++
                    //TODO startIndex
//                    while (startIndex <= nums.size) {
//                        nums[startIndex] = nums[startIndex - k]
//                    }
                    for (i in index..nums.size-1) {
                        nums[i-k+1] = nums[i]
                    }
                    for (i in nums.size-(k-1)..nums.size-1) {
                        nums[i] = Int.MAX_VALUE // analogue of '_'
                    }
                    println("$nums ${nums.contentToString()}")
                    println("")
                }
                index++
            }

            cnt = 0
            for (i in nums.size-1 downTo 0) {
                if (nums[i] == Int.MAX_VALUE) {
                    cnt++
                } else {
                    break
                }
            }
            return cnt
        }

    }
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val nums: IntArray = intArrayOf(1, 1, 1, 2, 2, 3)//, 4)//(0,0,1,1,1,2,2,3,3,4)////(1, 1, 2)
            println("nums = ${nums.contentToString()}")
            val res = Solution()
            val result = res.removeDuplicates(nums)
            println("res = $result nums = ${nums.contentToString()}")
//            for (i in nums) {
//                println("element $i")
//            }
        }
    }

}