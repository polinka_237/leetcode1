//Given a non-empty array of integers nums,
// every element appears twice except for one. Find that single one.
//
//You must implement a solution with a linear runtime
// complexity and use only constant extra space
//https://leetcode.com/problems/single-number/
class SingleNumber {
    class Solution {
        fun singleNumber(nums: IntArray): Int {
//            nums.sort()
//            var tmp: Int? = null
//            for (i in 1 until nums.size) {
//                if (nums[i] != nums[i-1]) {
//                    tmp = nums[i-1]
//                }
//            }
//            return tmp!!
            val list = mutableListOf<Int>()

            for (num in nums) {
                if (!list.contains(num)){
                    list.add(num)
                } else {
                    list.remove(num)
                }
            }

            return list.get(0)
        }
    }
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val nums: IntArray = intArrayOf(4,1,2,1,2)
            println("nums = ${nums.contentToString()}")
            val res = Solution()
            val result = res.singleNumber(nums)
            println("res = $result nums = ${nums.contentToString()}")
        }
    }
}