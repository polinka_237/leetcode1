import ForTasks.ListNode
//24 in leetcode
//https://leetcode.com/problems/swap-nodes-in-pairs/
//Given a linked list, swap every two adjacent nodes and return its head.
//You must solve the problem without modifying the values in the list's nodes (i.e., only nodes themselves may be changed.)

class SwapNodesInPairs {
    class Solution {

        fun swapPairs(head: ListNode?): ListNode? {
            if (head?.next == null) {
                return head
            }
            var ret = head
            val retValue = head?.next
            var prev : ListNode? = null
            while (ret?.next != null) {
                var first = ret
                var second = first?.next
                var third = second?.next

                first?.next = third
                second?.next = first
                prev?.next = second

                println("prev = ${prev?.toStringSimple()} first = ${first?.toStringSimple()} second=${second?.toStringSimple()} third=${third?.toStringSimple()}")
                prev = first
                ret = third
            }
            return retValue
        }

        fun swapPairs2(head: ListNode?): ListNode? {
            var ret = head
            val retValue = head?.next
//            var prev : ListNode? = null
            var first = ret
            var second = first?.next
            while (ret?.next != null) {
                first?.next = second?.next
                second?.next = first

                ret = ret?.next
            }
            return retValue
        }
    }
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            var node = ListNode(2)
            node.next = ListNode(4)
            node.next?.next = ListNode(6)
            node.next?.next?.next = ListNode(7)
            node.next?.next?.next?.next = ListNode(8)
            node.next?.next?.next?.next?.next = ListNode(9)

            println("node = $node")
            val res = Solution()
            val result = res.swapPairs(node)
            println("$result")

        }
    }
}