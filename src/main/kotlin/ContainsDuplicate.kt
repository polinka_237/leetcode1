//https://leetcode.com/problems/contains-duplicate/
//Given an integer array nums, return true if any value appears
// at least twice in the array, and return false if every element is distinct.
class ContainsDuplicate {
    class Solution {
        fun containsDuplicate(nums: IntArray): Boolean {
            nums.sort()
            for (i in 1 until nums.size) {
                if (nums[i] == nums[i-1]) return true
            }
            return false
        }
    }
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val nums: IntArray = intArrayOf(1 ,2, 1, 3)
            println("nums = ${nums.contentToString()}")
            val res = Solution()
            val result = res.containsDuplicate(nums)
            println("res = $result nums = ${nums.contentToString()}")
        }
    }
}