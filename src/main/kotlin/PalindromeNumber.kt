import kotlin.math.roundToInt

//https://leetcode.com/problems/palindrome-number/
class PalindromeNumber {
    class Solution {
        fun isPalindrome(value: Int): Boolean {


            val list = mutableListOf<Int>()
            var degree = 0
            var prev = 0
            while (true) {
                // next degree of 10
                val currentDegree = (Math.pow(10.0, (degree+1).toDouble())).roundToInt()
                // this is same value without remainder of division, remainder is moved out after div and multiply operations
                var tmp = value.div(currentDegree)*currentDegree//(value / currentDegree) * currentDegree

                // next digit, but with additional 0's
                var nextDigit = value - tmp - prev
                // cut value to the digit [0-9]
                nextDigit = nextDigit / Math.pow(10.0, degree.toDouble()).roundToInt()
                //previous is summ of all remainders
                prev += nextDigit * Math.pow(10.0, degree.toDouble()).roundToInt()
                list.add(nextDigit)
                degree++
                if (currentDegree > value) {
                    break
                }
            }

            for (i in 0..(list.size/2)) {
                if (list[i] != list[list.size-1-i]) {
                    return false
                }
            }
            return true
        }
    }
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val x= 1234321
            val res = Solution()
            val ret = res.isPalindrome(x)
            println("$x is palindrom = $ret")
        }
    }
}

