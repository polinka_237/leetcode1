import ForTasks.ListNode
import java.util.*

//https://leetcode.com/problems/merge-k-sorted-lists/
//23 in leetcode
//You are given an array of k linked-lists lists,
// each linked-list is sorted in ascending order.
//
//Merge all the linked-lists into one sorted
// linked-list and return it.

/**
 * Example:
 * var li = ListNode(5)
 * var v = li.`val`
 * Definition for singly-linked list.
 * class ListNode(var `val`: Int) {
 *     var next: ListNode? = null
 * }
 */

class MergeKsortedLists {
    class Solution {
        fun mergeKLists(lists: Array<ListNode?>): ListNode? {
            var head : ListNode? = ListNode(0)
            var ret = head
            if (lists.isEmpty()) return null
            else if (lists.size == 1) return lists.first()
//            for (i in 0 until lists.size+1) {
//                val res = mergeTwo(lists[i], lists[i+1])
//                tmp = res
//            }
//           return tmp

            var notNull = true
            while (notNull) {
                var tmp = Integer.MAX_VALUE
                var index = -1
                for (i in 0 until lists.size) {
                    if (lists[i] != null && tmp >= lists[i]!!.value) {
                        tmp = lists[i]!!.value
                        index = i
                        ret?.next = lists[i]
                    }
                }
                if (index == -1) {
                    break
                }
                // here we got minimum value and index of corresponding list
                lists[index] = lists[index]?.next
                //update ret to next element
                ret = ret?.next

                notNull = false
                for (element in lists) {
                    if (element != null) {
                        notNull = true
                        break
                    }
                }
            }
            return head?.next
        }

        fun mergeKListsElse(lists: Array<ListNode?>): ListNode? {
            val k = lists.size + 1
            val minHeap = PriorityQueue<ListNode>(k, compareBy( { it.value} ))
            lists.forEach { headNode ->
                if (headNode != null) minHeap.offer(headNode)
            }

            var result: ListNode? = null
            var resultBuilder = result
            while (minHeap.size > 0) {
                val nextNode = minHeap.poll()
                if (result == null) {
                    result = nextNode
                    resultBuilder = nextNode
                } else {
                    resultBuilder!!.next = nextNode
                    resultBuilder = nextNode
                }
                if (nextNode.next != null) {
                    minHeap.offer(nextNode.next)
                }
            }

            return result
        }

        fun mergeTwo(list1 : ListNode?, list2: ListNode?): ListNode? {
//            if (list1 != null || list2 != null) {
//                if (list1 != list2)
//            }
            return if (list1?.value!! < list2?.value!!) {
                list1.next = mergeTwo(list1.next, list2)
                list1
            }
            else {
                list2.next = mergeTwo(list1, list2.next)
                list2
            }
        }

    }


    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            var first = ListNode(2)
            first.next = ListNode(6)
            first.next?.next = ListNode(7)
            println("$first")
            var head2 = ListNode(4)
            head2.next = ListNode(8)
            head2.next?.next = ListNode(9)
            println("$head2")
            var head3 = ListNode(1)
            head3.next = ListNode(3)
            head3.next?.next = ListNode(5)
            head3.next?.next?.next = ListNode(6)
            println("$head3")

            val arr = arrayOf<ListNode?>(first, head2, head3)
            //val arr = arrayOf<ListNode?>()

            println("$arr")
            val res = Solution()
            val result = res.mergeKListsElse(arr)
            println("$result")

        }
    }

}