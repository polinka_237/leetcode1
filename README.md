# LeetCode1

Решенные задачи:

9 easy, Palindrome Number https://leetcode.com/problems/palindrome-number/

23 hard, Merge k Sorted Lists https://leetcode.com/problems/merge-k-sorted-lists/

24 medium, Swap Nodes in Pairs https://leetcode.com/problems/swap-nodes-in-pairs/

26 easy, RemoveDuplicatesFromSortedArray https://leetcode.com/problems/remove-duplicates-from-sorted-array/

27 easy, Remove Element https://leetcode.com/problems/remove-element/

66 easy, Plus One https://leetcode.com/problems/plus-one/

67 easy, AddBinary https://leetcode.com/problems/add-binary/

80 medium, Remove Duplicates from Sorted Array II https://leetcode.com/problems/remove-duplicates-from-sorted-array-ii/

136 easy, Single Number https://leetcode.com/problems/single-number/

189 medium, Rotate Array https://leetcode.com/problems/rotate-array/

217 easy. Contains Duplicate https://leetcode.com/problems/contains-duplicate/



